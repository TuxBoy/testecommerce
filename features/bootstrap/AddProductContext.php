<?php

use Behat\Behat\Context\Context;

class AddProductContext implements Context
{
    /**
     * @Given /^I want add product to my cart$/
     */
    public function iWantAddProductToMyCart()
    {
        throw new \Behat\Behat\Tester\Exception\PendingException();
    }

    /**
     * @Given /^The product is available in stock$/
     */
    public function theProductIsAvailableInStock()
    {
        throw new \Behat\Behat\Tester\Exception\PendingException();
    }

    /**
     * @When /^I save the product$/
     */
    public function iSaveTheProduct()
    {
        throw new \Behat\Behat\Tester\Exception\PendingException();
    }

    /**
     * @Then /^The product is available in my basket and the client can to order and the product stock is updated$/
     */
    public function theProductIsAvailableInMyBasketAndTheClientCanToOrderAndTheProductStockIsUpdated()
    {
        throw new \Behat\Behat\Tester\Exception\PendingException();
    }
}