Feature: Add product to cart

  Scenario: As a Visitor I want add product to cart
    Given I want add product to my cart
    And The product is available in stock
    When I save the product
    Then The product is available in my basket and the client can to order and the product stock is updated