Feature: Delete a product from the cart

  Scenario: As a Visitor I want delete a product from the cart
    Given I want remove product of my cart
    When I remove the product
    Then The product should not be displayed in my cart