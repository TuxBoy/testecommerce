<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Entity;

use App\Infrastructure\Doctrine\Traits\HasTimestamps;
use App\Infrastructure\Doctrine\Traits\HasUuid;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Infrastructure\Adapter\Repository\ProductRepository;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @UniqueEntity(fields={"name", "slug"})
 */
class DoctrineProduct
{
    use HasUuid;
    use HasTimestamps;

    /**
     * @ORM\Column
     */
    private string $name;

    /**
     * @ORM\Column(unique=true)
     */
    private string $slug;

    /**
     * @ORM\Column(type="integer")
     */
    private int $price;

    /**
     * @ORM\Column(type="integer")
     */
    private int $stock;

    /**
     * @ORM\ManyToOne(targetEntity=DoctrineCategory::class, inversedBy="products")
     */
    private ?DoctrineCategory $category;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return DoctrineProduct
     */
    public function setName(string $name): DoctrineProduct
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return DoctrineProduct
     */
    public function setSlug(string $slug): DoctrineProduct
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return DoctrineProduct
     */
    public function setPrice(int $price): DoctrineProduct
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     * @return DoctrineProduct
     */
    public function setStock(int $stock): DoctrineProduct
    {
        $this->stock = $stock;
        return $this;
    }

    public function getCategory(): ?DoctrineCategory
    {
        return $this->category;
    }

    public function setCategory(?DoctrineCategory $category): self
    {
        $this->category = $category;

        return $this;
    }
}
