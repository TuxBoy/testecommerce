<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Entity;

use App\Infrastructure\Doctrine\Traits\HasTimestamps;
use App\Infrastructure\Doctrine\Traits\HasUuid;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Infrastructure\Adapter\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 * @UniqueEntity("slug")
 */
class DoctrineCategory
{
    use HasUuid;
    use HasTimestamps;

    /**
     * @ORM\Column
     */
    private ?string $name = null;

    /**
     * @ORM\Column(unique=true)
     */
    private ?string $slug = null;

    /**
     * @ORM\OneToMany(targetEntity=DoctrineProduct::class, mappedBy="category")
     */
    private Collection $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function __toString(): string
    {
        return $this->name ?? '';
    }

    public function addProduct(DoctrineProduct $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
        }

        return $this;
    }

    public function removeProduct(DoctrineProduct $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): DoctrineCategory
    {
        $this->name = $name;
        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): DoctrineCategory
    {
        $this->slug = $slug;
        return $this;
    }
}
