<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\DataFixtures;

use App\Infrastructure\Doctrine\Entity\DoctrineCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixture extends Fixture
{
    public const CATEGORY_REFERENCE = 'category';

    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i <= 10; $i++) {
            $category = (new DoctrineCategory())
                ->setName('Category ' . $i)
                ->setSlug('category-' . $i)
                ->setUpdatedAt(new \DateTime())
                ->setCreatedAt(new \DateTime())
            ;
            $manager->persist($category);
            $this->addReference(self::CATEGORY_REFERENCE . '-' . $i, $category);
        }

        $manager->flush();
    }
}
