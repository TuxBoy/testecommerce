<?php

declare(strict_types=1);

namespace App\Infrastructure\Adapter\Repository;

use App\Domain\Shop\Entity\Product;
use App\Domain\Shop\Gateway\ProductGatewayInterface;
use App\Infrastructure\Doctrine\Entity\DoctrineCategory;
use App\Infrastructure\Doctrine\Entity\DoctrineProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository implements ProductGatewayInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DoctrineProduct::class);
    }

    public function create(Product $product): void
    {
        $doctrineCategory = $this->getEntityManager()
          ->getRepository(DoctrineCategory::class)
          ->find($product->getCategory()->getUuid());

        $doctrineProduct = (new DoctrineProduct())
            ->setId($product->getUuid())
            ->setName($product->getName())
            ->setSlug($product->getSlug())
            ->setPrice($product->getPrice())
            ->setStock($product->getStock())
            ->setUpdatedAt($product->getUpdatedAt())
            ->setCategory($doctrineCategory)
        ;

        $this->getEntityManager()->persist($doctrineProduct);
        $this->getEntityManager()->flush();
    }

    /**
     * @inheritDoc
     */
    public function checkUniqueSlug(?string $slug): bool
    {
        return $this->count(['slug' => $slug]) === 0;
    }

    /**
     * @inheritDoc
     */
    public function getAllProducts(): array
    {
        return [];
    }
}
