<?php

declare(strict_types=1);

namespace App\Infrastructure\Adapter\Repository;

use App\Domain\Shop\Entity\Category;
use App\Domain\Shop\Gateway\CategoryGatewayInterface;
use App\Infrastructure\Doctrine\Entity\DoctrineCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\UuidInterface;

class CategoryRepository extends ServiceEntityRepository implements CategoryGatewayInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DoctrineCategory::class);
    }

    /**
     * @param Category $category
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(Category $category): void
    {
        $categoryDoctrine = (new DoctrineCategory())
            ->setId($category->getUuid())
            ->setName($category->getName())
            ->setSlug($category->getSlug())
            ->setUpdatedAt($category->getUpdatedAt())
        ;
        $this->getEntityManager()->persist($categoryDoctrine);
        $this->getEntityManager()->flush();
    }

    public function update(Category $category): Category
    {
        return $category;
    }

    public function findById(UuidInterface $uuid): ?Category
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function checkUniqueSlug(?string $slug): bool
    {
        return $this->count(['slug' => $slug]) === 0;
    }
}
