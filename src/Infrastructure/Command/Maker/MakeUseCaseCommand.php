<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\Maker;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Twig\Environment;

class MakeUseCaseCommand extends Command
{
    protected static $defaultName = 'do:use-case';

    private string $namespace = "App\\Domain";

    public function __construct(private Environment $twig, protected string $projectDir)
    {
        parent::__construct(static::$defaultName);
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Created useCase class with Response, Presenter and Request associated.')
            ->addArgument('useCaseName', InputArgument::REQUIRED, 'The useCase name')
            ->addArgument('domain', InputArgument::OPTIONAL, 'The domain name')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $domain = $input->getArgument('domain');
        $useCaseName = $input->getArgument('useCaseName');
        if (!is_string($useCaseName) || ($domain !== null && !is_string($domain))) {
            throw new \InvalidArgumentException('The useCase must be string type.');
        }

        if ($domain === null) {
            $domain = $this->askDomain($io);
        }

        $responseClassName = $this->askClass(
            sprintf('Response class name ? (%sResponse)', $useCaseName),
            $useCaseName,
            'Response',
            $io
        );
        $requestNamespace = null;
        $requestClassName = null;
        $question = new Question('Do you want Request class ? [Y/N]');
        $hasRequest = $io->askQuestion($question);
        if ($hasRequest === 'y' || $hasRequest === null || $hasRequest === 'Y') {
            $requestClassName = $this->askClass(
                sprintf('Request class name ? (%sRequest)', $useCaseName),
                $useCaseName,
                'Request',
                $io
            );
        }

        $presenterClassName = $this->askClass(
            sprintf('Presenter interface name ? (%sPresenterInterface)', $useCaseName),
            $useCaseName,
            'PresenterInterface',
            $io
        );

        [$responseNamespace, $responseClassName] =
          $this->createDomainClass('response', $responseClassName, $this->namespace, $domain);
        if ($requestClassName !== null) {
            [$requestNamespace, $requestClassName] =
              $this->createDomainClass('request', $requestClassName, $this->namespace, $domain);
        }

        [$presenterNamespace, $presenterClassName] = $this->createDomainClass(
            'presenter',
            $presenterClassName,
            $this->namespace,
            $domain,
            ['response_class_name' => $responseClassName, 'response_namespace' => $responseNamespace]
        );

        $params = [
              'class_name' => $useCaseName,
              'namespace' => $this->namespace,
              'domain_name' => $domain,
              'presenter_class_name' => $presenterClassName,
              'request_class_name' => $requestClassName,
              'response_class_name' => $responseClassName,
              'presenter_namespace' => $presenterNamespace,
              'response_namespace' => $responseNamespace,
              'request_namespace' => $requestNamespace,
              'use_case' => $this->namespace . '\\' . $domain . '\\UseCase\\' . $useCaseName,
        ];

        // Create use case class
        $this->createDomainClass('useCase', $useCaseName, $this->namespace, $domain, $params);

        $params['namespace'] = 'App\Tests\Unit\Domain\\' . $domain;
        $this->createFile('useCaseTest', $params, sprintf("tests/Unit/Domain/%s/%sTest.php", $domain, $useCaseName));

        $io->text('The generated classes :');
        $io->block(
            array_filter([$responseClassName, $requestClassName, $presenterClassName]),
            prefix: ' - ',
            escape: false
        );
        $io->success('The use case has been created.');
        return Command::SUCCESS;
    }

    private function askClass(string $question, string $useCaseName, string $type, SymfonyStyle $io): string
    {
        $question = new Question($question);
        return $io->askQuestion($question) ?? $useCaseName . $type;
    }

    /**
     * @param string $template
     * @param string $className
     * @param string $namespace
     * @param string $domain
     * @param array $params
     * @return string[]
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function createDomainClass(
        string $template,
        string $className,
        string $namespace,
        string $domain,
        array $params = []
    ): array {
        $namespace .= '\\' . $domain . '\\' . ucfirst($template);
        $className = str_replace('\\', '/', $className);
        if (str_contains($className, '/') === true) {
            [$finalNamespace, $className] = explode('/', $className);
            $namespace .= '\\' . $finalNamespace;
        }
        $path = str_replace('\\', '/', $namespace);
        $path = str_replace('App', 'src', $path);
        $path .= '/' . $className . '.php';

        $this->createFile($template, array_merge($params, [
              'class_name' => $className,
              'namespace' => $namespace,
              'domain_name' => $domain,
        ]), $path);

        return [$namespace, $className];
    }

    /**
     * @param string $template
     * @param array $params
     * @param string $output
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function createFile(string $template, array $params, string $output): void
    {
        $content = $this->twig->render("@maker/$template.twig", $params);
        $filename = "{$this->projectDir}/{$output}";
        $directory = dirname($filename);

        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }
        file_put_contents($filename, $content);
    }

    private function askDomain(SymfonyStyle $io): string
    {
        $domains = [];
        $files = (new Finder())->in("{$this->projectDir}/src/Domain")->depth(0)->directories();
        foreach ($files as $file) {
            $domains[] = $file->getBasename();
        }

        $question = new Question('Select domain');
        $question->setAutocompleterValues($domains);

        return $io->askQuestion($question);
    }
}
