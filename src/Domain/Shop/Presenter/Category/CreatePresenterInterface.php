<?php

declare(strict_types=1);

namespace App\Domain\Shop\Presenter\Category;

use App\Domain\Shop\Response\Category\CreateResponse;

interface CreatePresenterInterface
{
    public function present(CreateResponse $response): void;
}
