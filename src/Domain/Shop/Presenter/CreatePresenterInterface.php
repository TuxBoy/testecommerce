<?php

declare(strict_types=1);

namespace App\Domain\Shop\Presenter;

use App\Domain\Shop\Response\CreateResponse;

interface CreatePresenterInterface
{
    public function present(CreateResponse $response): void;
}
