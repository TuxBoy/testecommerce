<?php

declare(strict_types=1);

namespace App\Domain\Shop\Presenter;

use App\Domain\Shop\Response\ListProductResponse;

interface ListProductPresenterInterface
{
    public function present(ListProductResponse $response): void;
}
