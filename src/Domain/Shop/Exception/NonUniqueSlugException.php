<?php

declare(strict_types=1);

namespace App\Domain\Shop\Exception;

use Assert\InvalidArgumentException;

class NonUniqueSlugException extends InvalidArgumentException
{
}
