<?php

declare(strict_types=1);

namespace App\Domain\Shop\Request;

use App\Domain\Shop\Assert\Assertion;
use App\Domain\Shop\Entity\Category;
use App\Domain\Shop\Gateway\ProductGatewayInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class CreateRequest
{
    public function __construct(
        private UuidInterface $uuid,
        private string $name,
        private int $price,
        private string $slug,
        private int $stock,
        private Category $category,
    ) {
    }

    public static function create(string $name, int $price, string $slug, int $stock, Category $category): self
    {
        return new self(Uuid::uuid4(), $name, $price, $slug, $stock, $category);
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @throws \Assert\AssertionFailedException
     */
    public function validate(ProductGatewayInterface $productGateway): void
    {
        Assertion::notBlank($this->name, 'The name not must be empty.');
        Assertion::greaterThan($this->price, 0, 'The price must be greater than 0.');
        Assertion::notBlank($this->slug, 'The slug not must be empty.');
        Assertion::regex($this->slug, "#^[a-z0-9-]+$#", 'The slug format is invalid.');
        Assertion::uniqueSlug($this->slug, $productGateway);
    }
}
