<?php

declare(strict_types=1);

namespace App\Domain\Shop\Request\Category;

use App\Domain\Shop\Assert\Assertion;
use App\Domain\Shop\Gateway\CategoryGatewayInterface;

class CreateRequest
{
    public function __construct(
        private ?string $name,
        private ?string $slug,
    ) {
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function validate(CategoryGatewayInterface $categoryGateway): void
    {
        Assertion::notBlank($this->name, 'The category name can\'t blank.');
        Assertion::notBlank($this->slug, 'The category slug can\'t blank.');
        Assertion::regex($this->slug, "#^[a-z0-9-]+$#", 'The slug format is invalid.');
        Assertion::uniqueSlug($this->slug, $categoryGateway);
    }
}
