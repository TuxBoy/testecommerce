<?php

declare(strict_types=1);

namespace App\Domain\Shop\Assert;

use App\Domain\Shop\Exception\NonUniqueSlugException;
use App\Domain\Shop\Gateway\ProductGatewayInterface;
use App\Domain\Shop\Gateway\ShopGatewayInterface;

class Assertion extends \Assert\Assertion
{
    private const SLUG_CODE = 231;

    public static function uniqueSlug(?string $slug, ShopGatewayInterface $gateway): void
    {
        if ($gateway->checkUniqueSlug($slug) === false) {
            throw new NonUniqueSlugException(sprintf('The slug %s is non unique.', $slug), self::SLUG_CODE);
        }
    }
}
