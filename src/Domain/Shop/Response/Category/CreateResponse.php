<?php

declare(strict_types=1);

namespace App\Domain\Shop\Response\Category;

use App\Domain\Shop\Entity\Category;

class CreateResponse
{
    private Category $category;

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): CreateResponse
    {
        $this->category = $category;
        return $this;
    }
}
