<?php

declare(strict_types=1);

namespace App\Domain\Shop\Response;

use App\Domain\Shop\Entity\Product;

class ListProductResponse
{
    /**
     * ListProductResponse constructor.
     * @param array<int, Product> $products
     */
    public function __construct(private array $products = [])
    {
    }

    /**
     * @return array<int, Product>
     */
    public function getProducts(): array
    {
        return $this->products;
    }
}
