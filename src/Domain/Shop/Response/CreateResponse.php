<?php

declare(strict_types=1);

namespace App\Domain\Shop\Response;

use App\Domain\Shop\Entity\Product;

class CreateResponse
{
    private Product $product;

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
