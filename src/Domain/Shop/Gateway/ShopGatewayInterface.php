<?php

declare(strict_types=1);

namespace App\Domain\Shop\Gateway;

interface ShopGatewayInterface
{
    /**
     * Return true if the slug is unique
     *
     * @return bool
     */
    public function checkUniqueSlug(?string $slug): bool;
}
