<?php

declare(strict_types=1);

namespace App\Domain\Shop\Gateway;

use App\Domain\Shop\Entity\Category;
use Ramsey\Uuid\UuidInterface;

interface CategoryGatewayInterface extends ShopGatewayInterface
{
    public function create(Category $category): void;

    public function update(Category $category): Category;

    public function findById(UuidInterface $uuid): ?Category;
}
