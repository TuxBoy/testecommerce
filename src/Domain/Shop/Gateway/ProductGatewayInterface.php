<?php

declare(strict_types=1);

namespace App\Domain\Shop\Gateway;

use App\Domain\Shop\Entity\Product;

interface ProductGatewayInterface extends ShopGatewayInterface
{
    public function create(Product $product): void;

    /**
     * @return array<int, Product>
     */
    public function getAllProducts(): array;
}
