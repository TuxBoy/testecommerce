<?php

declare(strict_types=1);

namespace App\Domain\Shop\Entity;

use DateTime;
use Ramsey\Uuid\UuidInterface;

class Category
{
    private DateTime $createdAt;

    public function __construct(
        private UuidInterface $uuid,
        private ?string $name,
        private ?string $slug,
        private \DateTimeInterface $updatedAt,
    ) {
        $this->createdAt = new DateTime();
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }
}
