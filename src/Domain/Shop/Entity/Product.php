<?php

declare(strict_types=1);

namespace App\Domain\Shop\Entity;

use App\Domain\Shop\Request\CreateRequest;
use DateTime;
use Ramsey\Uuid\UuidInterface;

class Product
{
    private UuidInterface $uuid;

    private string $name;

    private int $price;

    private string $slug;

    private int $stock;

    private DateTime $createdAt;

    private DateTime $updatedAt;

    private Category $category;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    public static function fromRequest(CreateRequest $request): self
    {
        return (new self())
            ->setUuid($request->getUuid())
            ->setName($request->getName())
            ->setSlug($request->getSlug())
            ->setPrice($request->getPrice())
            ->setStock($request->getStock())
            ->setCategory($request->getCategory())
            ->setUpdatedAt(new DateTime())
        ;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function setUuid(UuidInterface $uuid): Product
    {
        $this->uuid = $uuid;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Product
    {
        $this->name = $name;
        return $this;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function setPrice(int $price): Product
    {
        $this->price = $price;
        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): Product
    {
        $this->slug = $slug;
        return $this;
    }

    public function getStock(): int
    {
        return $this->stock;
    }

    public function setStock(int $stock): Product
    {
        $this->stock = $stock;
        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): Product
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTime $updatedAt): Product
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): Product
    {
        $this->category = $category;
        return $this;
    }
}
