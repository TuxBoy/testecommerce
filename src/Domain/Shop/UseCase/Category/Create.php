<?php

declare(strict_types=1);

namespace App\Domain\Shop\UseCase\Category;

use App\Domain\Shop\Entity\Category;
use App\Domain\Shop\Gateway\CategoryGatewayInterface;
use App\Domain\Shop\Presenter\Category\CreatePresenterInterface;
use App\Domain\Shop\Request\Category\CreateRequest;
use App\Domain\Shop\Response\Category\CreateResponse;
use Ramsey\Uuid\Uuid;

class Create
{
    public function __construct(private CategoryGatewayInterface $gateway)
    {
    }

    public function execute(CreateRequest $request, CreatePresenterInterface $presenter): void
    {
        $request->validate($this->gateway);

        $category = new Category(
            Uuid::uuid4(),
            $request->getName(),
            $request->getSlug(),
            new \DateTime()
        );
        $this->gateway->create($category);

        $response = new CreateResponse();
        $response->setCategory($category);

        $presenter->present($response);
    }
}
