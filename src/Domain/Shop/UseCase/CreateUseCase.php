<?php

declare(strict_types=1);

namespace App\Domain\Shop\UseCase;

use App\Domain\Shop\Entity\Product;
use App\Domain\Shop\Gateway\ProductGatewayInterface;
use App\Domain\Shop\Presenter\CreatePresenterInterface;
use App\Domain\Shop\Request\CreateRequest;
use App\Domain\Shop\Response\CreateResponse;
use Assert\AssertionFailedException;

class CreateUseCase
{
    public function __construct(private ProductGatewayInterface $productGateway)
    {
    }

    /**
     * @param CreateRequest $request
     * @param CreatePresenterInterface $presenter
     * @throws AssertionFailedException
     */
    public function execute(CreateRequest $request, CreatePresenterInterface $presenter): void
    {
        $request->validate($this->productGateway);

        $product = Product::fromRequest($request);

        $this->productGateway->create($product);

        $response = (new CreateResponse())->setProduct($product);

        $presenter->present($response);
    }
}
