<?php

declare(strict_types=1);

namespace App\Domain\Shop\UseCase;

use App\Domain\Shop\Gateway\ProductGatewayInterface;
use App\Domain\Shop\Presenter\ListProductPresenterInterface;
use App\Domain\Shop\Response\ListProductResponse;

class ListProduct
{
    public function __construct(private ProductGatewayInterface $productGateway)
    {
    }

    public function execute(ListProductPresenterInterface $presenter): void
    {
        $products = $this->productGateway->getAllProducts();

        $response = new ListProductResponse($products);

        $presenter->present($response);
    }
}
