<?php

declare(strict_types=1);

namespace App\UserInterface\ViewModel;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class CreateProductViewModel
{
    public function __construct(
        private Environment $twig,
        private UrlGeneratorInterface $urlGenerator,
        private FlashBagInterface $flashBag,
    ) {
    }

    public function renderSuccess(): RedirectResponse
    {
        $this->flashBag->add('success', 'Le produit a bien été ajouté');
        return new RedirectResponse($this->urlGenerator->generate('home'));
    }
}
