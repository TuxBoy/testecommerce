<?php

declare(strict_types=1);

namespace App\UserInterface\ViewModel;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class CreateCategoryViewModel
{
    private ?string $name = null;

    public function __construct(
        private Environment $twig,
        private UrlGeneratorInterface $urlGenerator,
        private FlashBagInterface $flashBag,
    ) {
    }

    public function renderSuccess(): RedirectResponse
    {
        $this->flashBag->add('success', sprintf('The %s category has been created.', $this->name));
        return new RedirectResponse($this->urlGenerator->generate('home'));
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): CreateCategoryViewModel
    {
        $this->name = $name;
        return $this;
    }

    public function render(array $data = []): Response
    {
        return new Response($this->twig->render('category/create.html.twig', $data));
    }
}
