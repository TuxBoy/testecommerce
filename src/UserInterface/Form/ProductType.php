<?php

declare(strict_types=1);

namespace App\UserInterface\Form;

use App\Infrastructure\Doctrine\Entity\DoctrineCategory;
use App\UserInterface\Dto\ProductDto;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Nom :'])
            ->add('slug', TextType::class, ['label' => 'Url :'])
            ->add('price', NumberType::class, ['label' => 'Prix :'])
            ->add('stock', NumberType::class, ['label' => 'Stock :'])
            ->add('category', EntityType::class, [
                'class' => DoctrineCategory::class,
                'placeholder' => 'Sélectionner une catégory',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('data_class', ProductDto::class);
    }
}
