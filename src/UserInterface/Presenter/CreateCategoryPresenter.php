<?php

declare(strict_types=1);

namespace App\UserInterface\Presenter;

use App\Domain\Shop\Presenter\Category\CreatePresenterInterface;
use App\Domain\Shop\Response\Category\CreateResponse;
use App\UserInterface\ViewModel\CreateCategoryViewModel;

class CreateCategoryPresenter implements CreatePresenterInterface
{
    public function __construct(private CreateCategoryViewModel $viewModel)
    {
    }

    public function present(CreateResponse $response): void
    {
        $this->viewModel->setName($response->getCategory()->getName());
    }

    public function viewModel(): CreateCategoryViewModel
    {
        return $this->viewModel;
    }
}
