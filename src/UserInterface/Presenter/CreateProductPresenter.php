<?php

declare(strict_types=1);

namespace App\UserInterface\Presenter;

use App\Domain\Shop\Presenter\CreatePresenterInterface;
use App\Domain\Shop\Response\CreateResponse;
use App\UserInterface\ViewModel\CreateProductViewModel;

class CreateProductPresenter implements CreatePresenterInterface
{
    public function __construct(private CreateProductViewModel $viewModel)
    {
    }

    public function present(CreateResponse $response): void
    {
    }

    public function viewModel(): CreateProductViewModel
    {
        return $this->viewModel;
    }
}
