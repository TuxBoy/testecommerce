<?php

declare(strict_types=1);

namespace App\UserInterface\Dto;

use App\Infrastructure\Doctrine\Entity\DoctrineCategory;
use DateTimeInterface;
use Ramsey\Uuid\UuidInterface;

final class ProductDto
{
    public UuidInterface $uuid;

    public string $name;

    public int $price;

    public string $slug;

    public int $stock;

    public DateTimeInterface $createdAt;

    public DateTimeInterface $updatedAt;

    public DoctrineCategory $category;
}
