<?php

declare(strict_types=1);

namespace App\UserInterface\Dto;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class CategoryDto
{
    private UuidInterface $id;

    #[Assert\NotBlank]
    private ?string $name = null;

    #[Assert\NotBlank]
    private ?string $slug = null;

    private \DateTimeInterface $updatedAt;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setName(?string $name): CategoryDto
    {
        $this->name = $name;
        return $this;
    }

    public function setSlug(?string $slug): CategoryDto
    {
        $this->slug = $slug;
        return $this;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }
}
