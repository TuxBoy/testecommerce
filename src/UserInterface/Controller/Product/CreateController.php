<?php

declare(strict_types=1);

namespace App\UserInterface\Controller\Product;

use App\Domain\Shop\Entity\Category;
use App\Domain\Shop\Gateway\ProductGatewayInterface;
use App\Domain\Shop\Request\CreateRequest;
use App\Domain\Shop\UseCase\CreateUseCase;
use App\Infrastructure\Adapter\Repository\ProductRepository;
use App\UserInterface\Dto\ProductDto;
use App\UserInterface\Form\ProductType;
use App\UserInterface\Presenter\CreateProductPresenter;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

#[Route('/product/create', name: 'product_create', methods: ['GET', 'POST'])]
final class CreateController
{
    public function __construct(
        private Environment $twig,
        private FormFactoryInterface $formFactory,
    ) {
    }

    public function __invoke(Request $request, CreateUseCase $useCase, CreateProductPresenter $presenter): Response
    {
        $form = $this->formFactory->create(ProductType::class, new ProductDto())->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ProductDto $productData */
            $productData = $form->getData();
            $category = new Category(
                $productData->category->getId(),
                $productData->category->getName(),
                $productData->category->getSlug(),
                $productData->category->getUpdatedAt()
            );

            $createRequest = CreateRequest::create(
                $productData->name,
                $productData->price,
                $productData->slug,
                $productData->stock,
                $category
            );

            $useCase->execute($createRequest, $presenter);

            return $presenter->viewModel()->renderSuccess();
        }

        return new Response(
            $this->twig->render('product/create.html.twig', ['form' => $form->createView()])
        );
    }
}
