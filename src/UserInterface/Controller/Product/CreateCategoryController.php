<?php

declare(strict_types=1);

namespace App\UserInterface\Controller\Product;

use App\Domain\Shop\Request\Category\CreateRequest;
use App\Domain\Shop\UseCase\Category\Create;
use App\UserInterface\Dto\CategoryDto;
use App\UserInterface\Form\CategoryType;
use App\UserInterface\Presenter\CreateCategoryPresenter;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/category/create', name: 'category_create', methods: ['GET', 'POST'])]
class CreateCategoryController
{
    public function __construct(private FormFactoryInterface $formFactory)
    {
    }

    public function __invoke(Request $request, Create $useCase, CreateCategoryPresenter $presenter): Response
    {
        $form = $this->formFactory->create(CategoryType::class, new CategoryDto())->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var CategoryDto $categoryData */
            $categoryData = $form->getData();
            $createRequest = new CreateRequest(
                $categoryData->getName(),
                $categoryData->getSlug(),
            );
            $useCase->execute($createRequest, $presenter);

            $presenter->viewModel()->renderSuccess();
        }

        return $presenter->viewModel()->render(['form' => $form->createView()]);
    }
}
