<?php

declare(strict_types=1);

namespace App\UserInterface\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

#[Route('/', name: 'home', methods: ['GET'])]
class HomeController
{
    public function __construct(public Environment $twig)
    {
    }

    public function __invoke(): Response
    {
        return new Response($this->twig->render('home.html.twig'));
    }
}
