.PHONY: format
format: vendor/autoload.php
	php vendor/bin/phpcs --ignore=./src/Kernel.php
	php vendor/bin/phpcbf

.PHONY: lint
lint: vendor/autoload.php
	./bin/console lint:container
	./bin/console lint:twig
	./vendor/bin/phpstan analyse

.PHONY: dev
dev: vendor/autoload.php
	docker-compose up -d
	symfony serve -d
	./bin/console c:c # Clear the symfony cache
	yarn run dev-server

.PHONY: prepare_test
prepare_test: vendor/autoload.php
	php bin/console doctrine:database:drop --force --env=test
	php bin/console doctrine:database:create --env=test
	php bin/console doctrine:migration:migrate --env=test --no-interaction
	php bin/console doctrine:fixtures:load --env=test

.PHONY: prepare_dev
prepare_dev: vendor/autoload.php
	php bin/console doctrine:database:drop --force
	php bin/console doctrine:database:create
	php bin/console doctrine:migration:migrate --no-interaction
	php bin/console doctrine:fixtures:load

.PHONY: stop
stop:
	symfony server:stop
	docker-compose down

vendor/autoload.php: composer.lock
	composer install
	touch vendor/autoload.php