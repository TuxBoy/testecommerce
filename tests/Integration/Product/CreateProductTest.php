<?php

declare(strict_types=1);

namespace App\Tests\Integration\Product;

use App\Infrastructure\Doctrine\Entity\DoctrineCategory;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class CreateProductTest extends WebTestCase
{
    public function testSuccessful(): void
    {
        $client = static::createClient();

        $crawler = $client->request(Request::METHOD_GET, '/product/create');

        $this->assertResponseIsSuccessful();

        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');
        $categories = $entityManager->getRepository(DoctrineCategory::class)->findBy([], limit: 1);
        $form = $crawler->filter('form')->form();

        $token = $form->get('product')['_token']->getValue();

        $client->request(Request::METHOD_POST, '/product/create', [
            'product' => [
              '_token' => $token,
              'name' => 'name',
              'slug' => 'slug',
              'price' => 20,
              'stock' => 2,
              'category' => $categories[0]->getName()
            ]
        ]);

        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    /**
     * @dataProvider productFormProvider
     */
    public function testFailed(): void
    {
        $client = static::createClient();
        $crawler = $client->request(Request::METHOD_GET, '/product/create');
        $form = $crawler->filter('form')->form();

        $client->request(Request::METHOD_POST, '/product/create', [

        ]);
    }

    public function productFormProvider(): \Generator
    {
        yield [
          [
            'name' => 'name',
            'slug' => 'slug',
            'price' => 20,
            'stock' => 2,
            'category' => ''
          ]
        ];
    }
}
