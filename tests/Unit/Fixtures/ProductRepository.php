<?php

declare(strict_types=1);

namespace App\Tests\Unit\Fixtures;

use App\Domain\Shop\Entity\Category;
use App\Domain\Shop\Entity\Product;
use App\Domain\Shop\Gateway\ProductGatewayInterface;
use Ramsey\Uuid\Uuid;

class ProductRepository implements ProductGatewayInterface
{
    public function create(Product $product): void
    {
    }

    public function checkUniqueSlug(?string $slug): bool
    {
        return !in_array($slug, ['product-unique']);
    }

    /**
     * @inheritDoc
     */
    public function getAllProducts(): array
    {
        return [
          (new Product())
            ->setName('Product 1')
            ->setSlug('product-1')
            ->setStock(2)
            ->setPrice(200)
            ->setCategory(new Category(Uuid::uuid4(), 'Category 1', 'category-1', new \DateTime()))
            ->setUpdatedAt(new \DateTime())
        ];
    }
}
