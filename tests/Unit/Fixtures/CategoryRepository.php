<?php

declare(strict_types=1);

namespace App\Tests\Unit\Fixtures;

use App\Domain\Shop\Entity\Category;
use App\Domain\Shop\Gateway\CategoryGatewayInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class CategoryRepository implements CategoryGatewayInterface
{
    public function create(Category $category): void
    {
    }

    public function checkUniqueSlug(?string $slug): bool
    {
        return !in_array($slug, ['category-unique']);
    }

    public function update(Category $category): Category
    {
        return new Category(Uuid::uuid4(), 'Category 1', 'category-1', new \DateTime());
    }

    public function findById(UuidInterface $uuid): ?Category
    {
        return new Category(Uuid::uuid4(), 'Category 1', 'category-1', new \DateTime());
    }
}
