<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Shop;

use App\Domain\Shop\Entity\Category;
use App\Domain\Shop\Entity\Product;
use App\Domain\Shop\Presenter\CreatePresenterInterface;
use App\Domain\Shop\Request\CreateRequest;
use App\Domain\Shop\Response\CreateResponse;
use App\Domain\Shop\UseCase\CreateUseCase;
use App\Tests\Unit\Fixtures\ProductRepository;
use Assert\AssertionFailedException;
use DateTime;
use Generator;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class CreateUseCaseTest extends TestCase
{
    private CreateUseCase $useCase;

    private CreatePresenterInterface $presenter;

    protected function setUp(): void
    {
        parent::setUp();

        $this->presenter = new class () implements CreatePresenterInterface {
            public CreateResponse $response;
            public function present(CreateResponse $response): void
            {
                $this->response = $response;
            }
        };

        $this->useCase = new CreateUseCase(new ProductRepository());
    }

    public function testSuccessful(): void
    {
        $request = new CreateRequest(
            Uuid::uuid4(),
            'Shop 1',
            20,
            'product-1',
            2,
            new Category(Uuid::uuid4(), 'Category 1', 'category-1', new DateTime())
        );
        $this->useCase->execute($request, $this->presenter);

        $this->assertInstanceOf(CreateResponse::class, $this->presenter->response);
        $this->assertInstanceOf(Product::class, $this->presenter->response->getProduct());
        $this->assertEquals('Shop 1', $this->presenter->response->getProduct()->getName());
        $this->assertInstanceOf(Category::class, $this->presenter->response->getProduct()->getCategory());
    }

    /**
     * @dataProvider productDataProvider
     */
    public function testFailed(
        string $name,
        int $price,
        string $slug,
        int $stock,
        Category $category,
        string $errorMessage
    ): void {
        $this->expectException(AssertionFailedException::class);
        $this->expectExceptionMessage($errorMessage);

        $request = new CreateRequest(Uuid::uuid4(), $name, $price, $slug, $stock, $category);
        $this->useCase->execute($request, $this->presenter);
    }

    public function productDataProvider(): Generator
    {
        yield [
            '',
            20,
            'product-1',
            2,
            new Category(Uuid::uuid4(), 'Category 1', 'category-1', new DateTime()),
            'The name not must be empty.'
        ];
        yield [
            'Shop 1',
            0,
            'product-1',
            2,
            new Category(Uuid::uuid4(), 'Category 1', 'category-1', new DateTime()),
            'The price must be greater than 0.'
        ];
        yield [
            'Shop 1',
            20,
            '',
            2,
            new Category(Uuid::uuid4(), 'Category 1', 'category-1', new DateTime()),
            'The slug not must be empty.'
        ];
        yield [
            'Shop 1',
            20,
            'Shop 1',
            2,
            new Category(Uuid::uuid4(), 'Category 1', 'category-1', new DateTime()),
            'The slug format is invalid.'
        ];
        yield [
            'Shop uniq',
            20,
            'product-unique',
            2,
            new Category(Uuid::uuid4(), 'Category 1', 'category-1', new DateTime()),
            'The slug product-unique is non unique.'
        ];
    }
}
