<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Shop;

use App\Domain\Shop\Entity\Category;
use App\Domain\Shop\Entity\Product;
use App\Domain\Shop\Presenter\ListProductPresenterInterface;
use App\Domain\Shop\Response\ListProductResponse;
use App\Domain\Shop\UseCase\ListProduct;
use App\Tests\Unit\Fixtures\ProductRepository;
use PHPUnit\Framework\TestCase;

final class ListProductTest extends TestCase
{
    private ListProduct $useCase;

    private ListProductPresenterInterface $presenter;

    protected function setUp(): void
    {
        parent::setUp();

        $this->presenter = new class () implements ListProductPresenterInterface {
            public ListProductResponse $response;
            public function present(ListProductResponse $response): void
            {
                $this->response = $response;
            }
        };

        $this->useCase = new ListProduct(new ProductRepository());
    }

    public function testSuccessful(): void
    {
        $this->useCase->execute($this->presenter);

        $this->assertInstanceOf(ListProductResponse::class, $this->presenter->response);
        $this->assertNotEmpty($this->presenter->response->getProducts());
        $this->assertCount(1, $this->presenter->response->getProducts());
        $this->assertContainsOnlyInstancesOf(
            Category::class,
            array_map(fn (Product $product) => $product->getCategory(), $this->presenter->response->getProducts())
        );
        $this->assertEquals(
            ['Product 1'],
            array_map(fn (Product $product) => $product->getName(), $this->presenter->response->getProducts())
        );
    }
}
