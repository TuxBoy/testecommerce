<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Shop\Category;

use App\Domain\Shop\Entity\Category;
use App\Domain\Shop\Presenter\Category\CreatePresenterInterface;
use App\Domain\Shop\Request\Category\CreateRequest;
use App\Domain\Shop\Response\Category\CreateResponse;
use App\Domain\Shop\UseCase\Category\Create;
use App\Tests\Unit\Fixtures\CategoryRepository;
use Assert\AssertionFailedException;
use PHPUnit\Framework\TestCase;

final class CreateTest extends TestCase
{
    private Create $useCase;

    private CreatePresenterInterface $presenter;

    protected function setUp(): void
    {
        parent::setUp();

        $this->presenter = new class () implements CreatePresenterInterface {
            public CreateResponse $response;
            public function present(CreateResponse $response): void
            {
                $this->response = $response;
            }
        };

        $this->useCase = new Create(new CategoryRepository());
    }

    public function testSuccessful(): void
    {
        $request = new CreateRequest('Category 1', 'category-1');
        $this->useCase->execute($request, $this->presenter);

        $this->assertInstanceOf(CreateResponse::class, $this->presenter->response);
        $this->assertInstanceOf(Category::class, $this->presenter->response->getCategory());
        $this->assertEquals('Category 1', $this->presenter->response->getCategory()->getName());
        $this->assertNotNull($this->presenter->response->getCategory()->getUuid());
    }

    /**
     * @dataProvider categoryDataProvider
     */
    public function testFailed(string $name, string $slug): void
    {
        $this->expectException(AssertionFailedException::class);

        $request = new CreateRequest($name, $slug);
        $this->useCase->execute($request, $this->presenter);
    }

    public function categoryDataProvider(): \Generator
    {
        yield ['', 'category-1'];
        yield ['Category 1', ''];
        yield ['Category 1', 'invalid slug'];
        yield ['Category 1', 'category-unique'];
    }
}
